# Dev dla UX - logika w nawigacji

## Jak przebiega rozwój aplikacji

1. Mapowanie contentu: [070947f](https://bitbucket.org/BKrzysztof/dev-dla-ux/commits/2f6d19bb1f8e51cffefb6cc88114418cb7d95c78)

![alt text](./readmeImages/mappingContent.png)

2. Hook useCollapse: [c92b0f7](https://bitbucket.org/BKrzysztof/dev-dla-ux/commits/c92b0f7710a8704d7e4be7d32cfbba2e670efd94)

![alt text](./readmeImages/useCollapse.png)

2. Śledzenie pozycji scrolla: [3ada2e7](https://bitbucket.org/BKrzysztof/dev-dla-ux/commits/3ada2e7861c17f260b2982c10354dcad9afed7bd)

![alt text](./readmeImages/trackPosition.png)

4. Zasada 1 otwartego elementu: [9bedaa8](https://bitbucket.org/BKrzysztof/dev-dla-ux/commits/9bedaa80375251dbc2d0b2ffa7c7f51c095802db)

![alt text](./readmeImages/oneOpen.png)

## [wersja live](https://teeny-tiny-hot.surge.sh/)
