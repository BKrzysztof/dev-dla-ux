import { data } from "../data";
import Content from "./Content";
import { ViewProvider } from "./context";
import Nav from "./Nav";

const App = () => {
  return (
    <div className="app">
      <ViewProvider>
        <Nav {...{ data }} />
        <Content {...{ data }} />
      </ViewProvider>
    </div>
  );
};

export default App;
