import View from "./View";

const Content = ({ data }) => {
  const views = data.map((main, index) =>
    Array.isArray(main) ? (
      main.map((sub) => <View info={index + "." + (sub + 1)} />)
    ) : (
      <View info={index + ".0"} />
    )
  );
  return views;
};

export default Content;
