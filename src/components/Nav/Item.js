import { useContext } from "react";
import { ViewCtx } from "../context";

const Item = ({ type, main, sub }) => {
  const href = `#${main}-${sub !== undefined ? sub + 1 : 0}`;

  const { currentView } = useContext(ViewCtx);

  const pointer =
    href === currentView ? <span className="pointer">{type}</span> : null;

  return (
    <li className={`nav__item--${type}`}>
      {pointer}
      <a className="nav__link" href={href}>
        {main}.{sub !== undefined ? sub + 1 : 0}
      </a>
    </li>
  );
};

export default Item;
