import { useContext, useEffect } from "react";
import { ViewCtx } from "../context";
import Item from "./Item";
import Sub from "./Sub";

const Main = ({ item, index }) => {
  const { currentView, setOpen } = useContext(ViewCtx);

  useEffect(() => {
    const shouldBeOpen =
      currentView.substr(0, ("#" + index).length) === `#${index}`;
    if (shouldBeOpen) {
      setOpen("" + index);
    }
  }, [currentView]); //eslint-disable-line react-hooks/exhaustive-deps

  const component = Array.isArray(item) ? (
    <Sub {...{ item, main: index }} />
  ) : (
    <Item type="main" main={item} />
  );

  return <ul className="nav__list--main">{component}</ul>;
};
export default Main;
