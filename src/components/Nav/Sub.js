import { useContext } from "react";
import useCollapse from "../../hooks/useCollapse";
import { ViewCtx } from "../context";
import Item from "./Item";

const Sub = ({ item, main }) => {
  const { currentView, open, setOpen } = useContext(ViewCtx);

  const { ref: collapseRef, isExpanded } = useCollapse(open !== "" + main);

  const items = item.map((sub) => <Item type="sub" {...{ main, sub }} />);

  const mainPointer =
    "#" + main === currentView.substr(0, ("#" + main).length) ? (
      <span className="pointer">main</span>
    ) : null;

  const openPointer = isExpanded ? (
    <span className="pointer--open pointer">open</span>
  ) : null;

  return (
    <li className="nav__item--main">
      <button
        onClick={() => {
          isExpanded ? setOpen("") : setOpen("" + main);
        }}
      >
        {openPointer}
        {mainPointer}
        {main}.0
      </button>
      <ul className="nav__list--sub" ref={collapseRef}>
        {items}
      </ul>
    </li>
  );
};
export default Sub;
