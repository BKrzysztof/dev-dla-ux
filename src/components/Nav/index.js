import Main from "./Main";

const Nav = ({ data }) => {
  const items = data.map((item, index) => <Main {...{ item, index }} />);
  return <ul className="nav">{items}</ul>;
};

export default Nav;
