import { generateRandomColor } from "../utils";
import { observe } from "react-intersection-observer";
import { useContext, useMemo, useCallback } from "react";
import { ViewCtx } from "./context";

const View = ({ info }) => {
  const id = info.replace(".", "-");
  const { setCurrentView } = useContext(ViewCtx);
  const color = useMemo(generateRandomColor, []);

  const observerRef = useCallback(
    (node) => {
      if (!node) return;
      const cb = (inView) => {
        if (inView) setCurrentView("#" + id);
      };
      const threshold =
        window.innerHeight / node.getBoundingClientRect().height / 2;
      const destroy = observe(node, cb, {
        threshold: threshold >= 0 && threshold <= 1 ? threshold : 0,
      });
      return () => destroy();
    },
    [id] // eslint-disable-line react-hooks/exhaustive-deps
  );

  return (
    <div
      className="view"
      style={{ backgroundColor: color }}
      id={id}
      ref={observerRef}
    >
      <h2 className="view__info">{info}</h2>
    </div>
  );
};

export default View;
