import { createContext, useState } from "react";

export const ViewCtx = createContext({
  currentView: "#0-0",
  setCurrentView: (a) => {},
  open: "1",
  setOpen: (a) => {},
});

export const ViewProvider = ({ children }) => {
  const [currentView, setCurrentView] = useState("#0-0");
  const [open, setOpen] = useState("1");

  return (
    <ViewCtx.Provider value={{ currentView, setCurrentView, open, setOpen }}>
      {children}
    </ViewCtx.Provider>
  );
};
