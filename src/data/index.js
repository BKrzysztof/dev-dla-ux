const main = new Array(5).fill(1).map((_, i) => ({ i }));

export const data = main.map(({ i }) =>
  i % 3 === 1 ? new Array(3).fill(0).map((_, i) => i) : i
);
