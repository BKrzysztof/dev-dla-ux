import { useCallback } from "react";

const useCollapse = (condition) => {
  const ref = useCallback(
    (el) => {
      if (!el) return;
      if (condition) {
        el.style.maxHeight = "0px";
      } else {
        el.style.maxHeight = el.scrollHeight + "px";
      }
    },
    [condition]
  );

  return {
    ref,
    isExpanded: !condition,
  };
};

export default useCollapse;
