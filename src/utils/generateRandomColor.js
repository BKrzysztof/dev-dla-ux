const bigRandom = (n) => Math.floor(Math.random() * n);

const generateRandomColor = () =>
  `rgb(${bigRandom(255)},${bigRandom(255)},${bigRandom(255)})`;

export default generateRandomColor;
